# Deploying Nginx on AWS with Terraform

This project exists only for me to learn deploying with Terraform and Ansible and GitLabCI.

NGINX page is available at <http://13.38.80.14:8080>.

## What is ready

VPC and EC2 instances, networks and security rules.
NGINX is installed in Docker container with bash script.
Full networks deploy is available in feature/flat branch.
In main branch modules are used. Existing one for VPC and networks and self-made one for EC2 instance.

## Todo

GitLabCI and replace bash script with Ansible.

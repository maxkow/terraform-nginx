terraform {
  required_version = ">= 1.4.6"
  # backend "s3" {
  #   bucket = "code-settings"
  #   key = "code-settings/tf-nginx/state.tfstate"
  #   region = "eu-west-3a"
  # }
}



provider "aws" {
  region = "eu-west-3"
}



# https://github.com/terraform-aws-modules/terraform-aws-vpc
module "tf-nginx-vpc" {
  source = "terraform-aws-modules/vpc/aws"

  name = "tf-nginx-vpc-1"
  cidr = var.vpc_cidr_block

  azs             = var.avail_zones
  public_subnets  = var.sn_cidr_blocks

  public_subnet_tags = {
    "Name" = "tf-nginx-sn-1${var.env_prefix}"
    "Terraform" = "true"
    "Environment" = "${var.env_prefix}"
  }

  tags = {
    "Name" = "tf-nginx-vpc-1${var.env_prefix}"
    "Terraform" = "true"
    "Environment" = "${var.env_prefix}"
  }
}

module "tf-nginx-ec2" {
  source = "./modules/ec2s"

  env_prefix      = var.env_prefix
  avail_zones      = var.avail_zones
  rsa_key         = var.rsa_key
  ec2_inst_type   = var.ec2_inst_type
  route_ip        = var.route_ip
  allow_ip        = var.allow_ip
  vpc_id          = module.tf-nginx-vpc.vpc_id
  subnet_id       = module.tf-nginx-vpc.public_subnets
  ami_image_name  = var.ami_image_name
}

# common
env_prefix = ""  # "-dev"
avail_zones = [ "eu-west-3a" ]

# vpc
vpc_cidr_block  = "10.0.0.0/16"
allow_ip        = "212.58.102.122/32"

# subnet
sn_cidr_blocks = [ "10.0.10.0/24" ]
route_ip      = "0.0.0.0/0"

# ec2
ec2_inst_type   = "t2.micro"
ami_image_name  = "al2023-ami-20*-x86_64"

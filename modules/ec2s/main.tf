resource "aws_security_group" "tf-nginx-sg-1" {
  vpc_id = var.vpc_id
  name = "tf-nginx-sg-1"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = [ var.allow_ip ]
    description = "ssh access"
  }

  ingress {
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = [ var.route_ip ]
    description = "http access"
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = [ var.route_ip ]
    prefix_list_ids = []
    description     = "outcoming traffic"
  }

  tags = {
    "Name" = "tf-nginx-sg-1${var.env_prefix}"
    "Terraform" = "true"
    "Environment" = "${var.env_prefix}"
  }
}

data "aws_ami" "amazon-linux-latest" {
  most_recent = true
  owners      = [ "amazon" ]
  
  filter {
    name    = "name"
    values  = [ var.ami_image_name ]
  }
}

resource "aws_key_pair" "tf-nginx-kp" {
  key_name    = "tf-nginx-inst-key-1"
  public_key  = file(var.rsa_key)
}

resource "aws_instance" "tf-nginx-inst-1" {
  ami           = data.aws_ami.amazon-linux-latest.id
  instance_type = var.ec2_inst_type

  vpc_security_group_ids  = [ aws_security_group.tf-nginx-sg-1.id ]
  subnet_id               = var.subnet_id[0]
  availability_zone       = var.avail_zones[0]

  associate_public_ip_address = true
  key_name                    = aws_key_pair.tf-nginx-kp.key_name

  # these 2 lines will be removed in the future and replaced with ci/cd
  user_data                   = file("./modules/ec2s/entry_script.sh")
  user_data_replace_on_change = true

  tags = {
    "Name" = "tf-nginx-inst-1${var.env_prefix}"
    "Terraform" = "true"
    "Environment" = "${var.env_prefix}"
  }
}

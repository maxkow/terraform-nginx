variable "env_prefix" {
  description = "environment prefix"
  type        = string
}

variable "avail_zones" {
  description = "availability zones"
  type        = list(string)
}

variable "rsa_key" {
  description = "rsa key file"
  type        = string
}

variable "ec2_inst_type" {
  description = "ec2 instance type"
  type        = string
}

variable "route_ip" {
  description = "ip for default route table"
  type        = string
}

variable "allow_ip" {
  description = "ip from which access is allowed"
  type        = string
}

variable "vpc_id" {
  description = "vpc id"
  type        = string
}

variable "subnet_id" {
  description = "subnet ids"
  type        = list(string)
}

variable "ami_image_name" {
  description = "image name for filtering ami"
  type        = string
}

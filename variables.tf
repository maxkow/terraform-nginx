variable "env_prefix" {
  description = "environment prefix"
  type        = string
}

variable "avail_zones" {
  description = "availability zones"
  type        = list(string)
}

variable "vpc_cidr_block" {
  description = "vpc cidr block"
  type        = string
}

variable "sn_cidr_blocks" {
  description = "subnet cidr blocks"
  type        = list(string)
}

variable "route_ip" {
  description = "ip for default route table"
  type        = string
}

variable "allow_ip" {
  description = "ip from which access is allowed"
  type        = string
}

variable "rsa_key" {
  description = "rsa key file"
  type        = string
}

variable "ami_image_name" {
  description = "image name for filtering ami"
  type        = string
}

variable "ec2_inst_type" {
  description = "ec2 instance type"
  type        = string
}
